resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "main"
  }
}

resource "aws_subnet" "public-subnet1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "public-main-1"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "gw"
  }
}

resource "aws_route_table" "rt" {
  vpc_id = aws_vpc.main.id

  route = [
    {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.gw.id
      carrier_gateway_id         = ""
      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      instance_id                = ""
      ipv6_cidr_block            = ""
      local_gateway_id           = ""
      nat_gateway_id             = ""
      network_interface_id       = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
    }
  ]

  tags = {
    Name = "rt"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.public-subnet1.id
  route_table_id = aws_route_table.rt.id
}

resource "aws_security_group" "sg" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.main.id

  ingress = [
    {
      description      = "TLS from VPC"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = []
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = false
    }
  ]

  egress = [
    {
      description      = "TLS from VPC"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = []
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = false
    }
  ]

  tags = {
    Name = "allow_tls"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  owners = ["099720109477"]
}

resource "aws_instance" "web" {
  ami           = "ami-00399ec92321828f5"
  instance_type = "t2.micro"
  key_name = "ritik1st"
  subnet_id = aws_subnet.public-subnet1.id

  vpc_security_group_ids = [aws_security_group.sg.id]
  tags = {
    Name = "HelloWorld"
  }
}
resource "aws_instance" "web2" {
  ami           = "ami-00399ec92321828f5"
  instance_type = "t2.micro"
  key_name = "ritik1st"
  subnet_id = aws_subnet.public-subnet1.id

  vpc_security_group_ids = [aws_security_group.sg.id]
  tags = {
    Name = "HelloWorld"
  }
}
resource "aws_instance" "web3" {
  ami           = "ami-00399ec92321828f5"
  instance_type = "t2.micro"
  key_name = "ritik1st"
  subnet_id = aws_subnet.public-subnet1.id

  vpc_security_group_ids = [aws_security_group.sg.id]
  tags = {
    Name = "HelloWorld"
  }
}
resource "aws_instance" "web4" {
  ami           = "ami-00399ec92321828f5"
  instance_type = "t2.micro"
  key_name = "ritik1st"
  subnet_id = aws_subnet.public-subnet1.id

  vpc_security_group_ids = [aws_security_group.sg.id]
  tags = {
    Name = "HelloWorld"
  }
}