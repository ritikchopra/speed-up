# # Create a new load balancer
# resource "aws_lb" "devops-intern-lb" {
#   name               = "devops-intern-lb"
#   load_balancer_type = "network"
#   subnets = [ "subnet-a1f706dc", "subnet-d0b6ef9c", "subnet-2ada6141"]

#   # allocation_id = aws_eip.lb.id

#   tags = {
#     Name = "foobar-terraform-lb"
#   }
# }

# resource "aws_lb_target_group" "http" {
#   name        = "http"
#   port        = "80"
#   protocol    = "TCP"
#   target_type = "ip"
#   vpc_id      = "vpc-7f24b414" # Referencing the default VPC
#   health_check {
#     enabled  = true
#     interval = 30
#     port     = 22
#   } 
#   stickiness {
#     enabled = false
#     type = "lb_cookie"
#    }
# }
# # Security group for lb

resource "aws_security_group" "lb-security-group" {
  name        = "lb-security-group"
  description = "Allow TLS inbound traffic"
  vpc_id      = "vpc-7f24b414"

  ingress = [
    {
      description      = "TLS from VPC"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = []
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = false
    }
  ]

  egress = [
    {
      description      = "TLS from VPC"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = []
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      security_groups = []
      self = false
    }
  ]

  tags = {
    Name = "allow_tls"
  }

  lifecycle {
    create_before_destroy = true
  }
}

# # # security group for lb instances

resource "aws_security_group" "lb-instances-security-group" {
  name        = "lb-instances-security-group"
  description = "Allow TLS inbound traffic"
  vpc_id      = "vpc-7f24b414"

  ingress = [
    {
      description      = "TLS from VPC"
      from_port        = 22
      to_port          = 80
      protocol         = "tcp"
      security_groups   = [ aws_security_group.lb-security-group.id ]
      cidr_blocks      = []
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      self = false
    }
  ]

  egress = [
    {
      description      = "TLS from VPC"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      security_groups  = []
      cidr_blocks      = []
      ipv6_cidr_blocks = []
      prefix_list_ids = []
      self = false
    }
  ]

  tags = {
    Name = "lb-instances-security-group"
  }

  lifecycle {
    create_before_destroy = true
  }
}