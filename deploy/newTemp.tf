resource "aws_network_interface" "foo1" {
  subnet_id   = aws_subnet.my_subnet.id
  private_ips = ["10.0.0.12"]

  tags = {
    Name = "primary_network_interfacea"
  }
}