# resource "aws_iam_role" "example" {
#   name = "example-role"

#   assume_role_policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Sid": "",
#       "Effect": "Allow",
#       "Principal": {
#         "Service": [
#           "codedeploy.amazonaws.com",
#           "ec2.amazonaws.com"
#         ]
#       },
#       "Action": "sts:AssumeRole"
#     }
#   ]
# }
# EOF
# }
# resource "aws_iam_role_policy" "AWSCodeDeployRole" {
#   role       = aws_iam_role.example.name
#   policy = <<POLICY
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Action": [
#         "cloudwatch:DescribeAlarms",
#         "ecs:CreateTaskSet",
#         "ecs:DeleteTaskSet",
#         "ecs:DescribeServices",
#         "ecs:UpdateServicePrimaryTaskSet",
#         "elasticloadbalancing:DescribeListeners",
#         "elasticloadbalancing:DescribeRules",
#         "elasticloadbalancing:DescribeTargetGroups",
#         "elasticloadbalancing:ModifyListener",
#         "elasticloadbalancing:ModifyRule",
#         "lambda:InvokeFunction",
#         "s3:GetObject",
#         "s3:GetObjectMetadata",
#         "s3:GetObjectVersion",
#         "sns:Publish"
#       ],
#       "Effect": "Allow",
#       "Resource": "*"
#     }
#   ]
# }
# POLICY
# }

# resource "aws_codedeploy_app" "example" {
#   compute_platform = "ECS"
#   name = "example-app"
# }

# resource "aws_codedeploy_deployment_group" "example" {
#   app_name              = aws_codedeploy_app.example.name
#   deployment_config_name = "CodeDeployDefault.ECSAllAtOnce"
#   deployment_group_name = "example"
#   service_role_arn      = aws_iam_role.example.arn

#   blue_green_deployment_config {
#     deployment_ready_option {
#       action_on_timeout = "CONTINUE_DEPLOYMENT"
#     }

#     terminate_blue_instances_on_deployment_success {
#       action                           = "TERMINATE"
#       termination_wait_time_in_minutes = 5
#     }
#   }

#   deployment_style {
#     deployment_option = "WITH_TRAFFIC_CONTROL"
#     deployment_type   = "BLUE_GREEN"
#   }

#   ecs_service {
#     cluster_name = aws_ecs_cluster.appscrip-devops-intern-ecs-cluster.name
#     service_name = aws_ecs_service.ecs-service.name
#   }

#   load_balancer_info {
#     target_group_pair_info {
#       prod_traffic_route {
#         listener_arns = [aws_lb_listener.listener.arn]
#       }

#       target_group {
#         name = aws_lb_target_group.target_group.name
#       }

#       target_group {
#         name = aws_lb_target_group.target_group1.name
#       }
#     }
#   }

#   auto_rollback_configuration {
#     enabled = true
#     events  = ["DEPLOYMENT_FAILURE"]
#   }

# }