# resource "aws_eip" "lb" {
#   vpc = true
# }
# resource "aws_acm_certificate" "cert" {
#   domain_name       = "*rk.com"
#   validation_method = "EMAIL"

#   tags = {
#     Environment = "cert"
#   }

#   lifecycle {
#     create_before_destroy = true
#   }
# }

# resource "aws_lb_listener" "http" {
#   load_balancer_arn = aws_lb.devops-intern-lb.arn # Referencing our load balancer
#   port              = "80"
#   protocol          = "TCP"
#   default_action {
#     type             = "forward"
#     target_group_arn = aws_lb_target_group.http.arn # Referencing our tagrte group
#   }
# }

# resource "aws_lb_listener" "http" {
#   load_balancer_arn = aws_lb.devops-intern-lb.arn
#   port              = "80"
#   protocol          = "TCP"

#   default_action {
#     type = "redirect"

#     redirect {
#       port        = "443"
#       protocol    = "HTTPS"
#       status_code = "HTTP_301"
#     }
#   }
# }

# resource "aws_lb_listener" "https" {
#   load_balancer_arn = aws_lb.devops-intern-lb.arn
#   port              = "443"
#   protocol          = "TLS"
#   ssl_policy        = "ELBSecurityPolicy-2016-08"
#   certificate_arn   = aws_acm_certificate.cert.arn

#   default_action {
#     type = "redirect"

#     redirect {
#       port              = "443"
#       protocol          = "HTTPS"
#       status_code       = "HTTP_301"
#     }
#   }
# }

# resource "aws_lb_listener_certificate" "lb-listener-certificate-https" {
#   listener_arn    = aws_lb_listener.http.arn
#   certificate_arn = aws_acm_certificate.cert.arn
# }