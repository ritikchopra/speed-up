#!/bin/bash

sudo apt install openjdk-8-jdk -y
sudo apt install apt-transport-https
wget -q -O - https://www.apache.org/dist/cassandra/KEYS | sudo apt-key add -
sudo sh -c 'echo "deb http://www.apache.org/dist/cassandra/debian 311x main" > /etc/apt/sources.list.d/cassandra.list'
sudo apt-get update
sudo apt install cassandra -y

# nodetool status
# cqlsh
# sudo  // edit listener add, rpc add, seeds, endpoint
# sudo nodetool status
# systemctl status cassandra
# systemctl restart cassandra
# systemctl stop cassandra
# rm -Rf ~/.cassandra
# sudo rm -rf /var/lib/cassandra/data/system/*