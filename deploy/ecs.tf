# resource "aws_ecr_repository" "appscrip-devops-intern-ecr-repository" {
#   name = "appscrip-devops-intern-ecr-repository"
# }

# resource "aws_ecs_cluster" "appscrip-devops-intern-ecs-cluster" {
#   name = "appscrip-devops-intern-ecs-clustere"
# }


# data "aws_iam_role" "ecs_task_execution_role" {
#   name = "ecsTaskExecutionRole"
# }

# resource "aws_ecs_task_definition" "ecs-task-definition" {
#   family                   = "ecs-task-definition" 
#   container_definitions = <<DEFINITION
#   [
#     {
#       "image": "${var.image}",
#       "cpu": 1024,
#       "memory": 2048,
#       "name": "hello-world-app",
#       "essential": true,
#       "networkMode": "awsvpc",
#       "portMappings": [
#         {
#           "containerPort": 3000,
#           "hostPort": 3000
#         }
#       ]
#     }
#   ]
#   DEFINITION
#   requires_compatibilities = ["FARGATE"] 
#   network_mode             = "awsvpc"  
#   execution_role_arn       = data.aws_iam_role.ecs_task_execution_role.arn  
#   memory                   = 2048        
#   cpu                      = 1024
# }

# resource "aws_ecs_service" "ecs-service" {
#   name            = "ecs-service"                         
#   cluster         = aws_ecs_cluster.appscrip-devops-intern-ecs-cluster.id             
#   task_definition = aws_ecs_task_definition.ecs-task-definition.arn 
#   launch_type     = "FARGATE"
#   desired_count   = 3 

#   load_balancer {
#     target_group_arn = aws_lb_target_group.target_group.arn 
#     container_name   = "hello-world-app"
#     container_port   = 3000 
#   }

#   network_configuration {
#     subnets = [ "subnet-a1f706dc", "subnet-d0b6ef9c", "subnet-2ada6141"]
#     assign_public_ip = true                                                # Providing our containers with public IPs
#     security_groups  = [aws_security_group.service_security_group.id] # Setting the security group
#   }

#   # deployment_controller {
#   #   type = "CODE_DEPLOY"
#   # }

#   depends_on = [ aws_lb_target_group.target_group ]
# }


# resource "aws_security_group" "service_security_group" {
#   ingress {
#     from_port = 0
#     to_port   = 0
#     protocol  = "-1"
#     # Only allowing traffic in from the load balancer security group
#     security_groups = [aws_security_group.load_balancer_security_group.id]
#   }

#   egress {
#     from_port   = 0 # Allowing any incoming port
#     to_port     = 0 # Allowing any outgoing port
#     protocol    = "-1" # Allowing any outgoing protocol 
#     cidr_blocks = ["0.0.0.0/0"] # Allowing traffic out to all IP addresses
#   }
#   lifecycle {
#     create_before_destroy = true
#   }
# }


# resource "aws_alb" "application_load_balancer" {
#   name               = "test-lb-tf" 
#   load_balancer_type = "application"
#   subnets = [ "subnet-a1f706dc", "subnet-d0b6ef9c", "subnet-2ada6141"]
  
#   security_groups = [aws_security_group.load_balancer_security_group.id]
# }

# # Creating a security group for the load balancer:
# resource "aws_security_group" "load_balancer_security_group" {
#   ingress {
#     from_port   = 80
#     to_port     = 80
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   egress {
#     from_port   = 0 # Allowing any incoming port
#     to_port     = 0 # Allowing any outgoing port
#     protocol    = "-1" # Allowing any outgoing protocol 
#     cidr_blocks = ["0.0.0.0/0"] # Allowing traffic out to all IP addresses
#   }
# }

# resource "aws_lb_target_group" "target_group" {
#   name        = "target-group"
#   port        = 80
#   protocol    = "HTTP"
#   target_type = "ip"
#   vpc_id      = "vpc-7f24b414" # Referencing the default VPC
#   health_check {
#     matcher = "200,301,302"
#     path = "/"
#   }
#   depends_on = [ aws_alb.application_load_balancer ]
# }

# resource "aws_lb_target_group" "target_group1" {
#   name        = "target-group1"
#   port        = 80
#   protocol    = "HTTP"
#   target_type = "ip"
#   vpc_id      = "vpc-7f24b414" # Referencing the default VPC
#   health_check {
#     matcher = "200,301,302"
#     path = "/"
#   }
#   depends_on = [ aws_alb.application_load_balancer ]
# }

# resource "aws_lb_listener" "listener" {
#   load_balancer_arn = aws_alb.application_load_balancer.arn # Referencing our load balancer
#   port              = "80"
#   protocol          = "HTTP"
#   default_action {
#     type             = "forward"
#     target_group_arn = aws_lb_target_group.target_group.arn # Referencing our tagrte group
#   }
# }