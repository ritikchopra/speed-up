#!/bin/bash
#Install erlang first

wget -O- https://packages.erlang-solutions.com/ubuntu/erlang_solutions.asc | sudo apt-key add -
echo "deb https://packages.erlang-solutions.com/ubuntu bionic contrib" | sudo tee /etc/apt/sources.list.d/rabbitmq.list
sudo apt update
sudo apt install erlang -y

#Install GitHub

sudo apt install -y libssl-dev
sudo apt-get install libsnappy-dev -y
sudo apt install build-essential -y
sudo apt install gcc
make CC=gcc

#Install venermq
git clone git://github.com/erlio/vernemq.git
cd vernemq
make rel

export PATH="/home/ubuntu/vernemq/_build/default/rel/vernemq/bin/vernemq/bin:$PATH"
