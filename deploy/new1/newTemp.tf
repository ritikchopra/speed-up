resource "aws_instance" "foo1" {
  ami           = "ami-00399ec92321828f5"
  instance_type = "t2.micro"

  subnet_id   = data.aws_subnet.selected.id
  private_ip = "10.0.0.22"
  credit_specification {
    cpu_credits = "unlimited"
  }
}

data "aws_subnet" "selected" {
  filter {
    name   = "tag:Name"
    values = ["primary_aws_subnet"]
  }
}

