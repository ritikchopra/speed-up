terraform {
  backend "s3" {
    bucket = "appscrip-devops-intern-practice"
// key specify the path of state maintai
    key = "state/new1/key"
    region = "us-east-2"
  }
}
provider "aws" {
  region = "us-east-2"
  shared_credentials_file = "$HOME/.aws/credentials"
  profile = "devops-intern"
}
variable image {
  type        = string
  default     = "temporaryrk/demo:latest"
}
