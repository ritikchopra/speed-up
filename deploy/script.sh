#!/bin/bash
sudo apt-get install rabbitmq-server -y
sudo rabbitmq-plugins enable rabbitmq_management
sudo rabbitmqctl add_user test_admin test_password 
sudo rabbitmqctl set_user_tags test_admin administrator
sudo rabbitmqctl set_permissions -p / test_admin “.*” “.*” “.*”
sudo cat /var/lib/rabbitmq/.erlang.cookie
# for slaves vi /var/lib/rabbitmq/.erlang.cookie
systemctl restart rabbitmq-server.service
sudo rabbitmqctl join_cluster rabbit@<private IP of Node1>
sudo rabbitmqctl start_app
