# # launch configuration 
# resource "aws_launch_configuration" "as_conf" {
#   name          = "web_config"
#   image_id      = data.aws_ami.ubuntu.id
#   instance_type = "t2.micro"

#   security_groups = [aws_security_group.lb-instances-security-group.id]
#   user_data = "!bin/bash\napt-get update\napt-get -y install net-tools\nMYIP=`ifconfig |grep -E'(inet addr:17`"
#   lifecycle {
#     create_before_destroy = true
#   }

# }
# ################################################################################################

# #auto scaling group
# resource "aws_autoscaling_group" "autoscaling-group" {
#   name                      = "autoscaling-group"
#   max_size                  = 3
#   min_size                  = 1
#   health_check_grace_period = 100
#   health_check_type         = "EC2"
#   desired_capacity          = 2
#   force_delete              = true
#   launch_configuration      = aws_launch_configuration.as_conf.name
#   vpc_zone_identifier       = ["subnet-2ada6141", "subnet-d0b6ef9c", "subnet-2ada6141"]

#   tag {
#     key                 = "foo"
#     value               = "bar"
#     propagate_at_launch = true
#   }
# }

# #aws auto scaling policy

# resource "aws_autoscaling_policy" "autoscaling-policy" {
#   name                   = "autoscaling-policy"
#   scaling_adjustment     = 1
#   adjustment_type        = "ChangeInCapacity"
#   policy_type            = "SimpleScaling"
#   cooldown               = 60
#   autoscaling_group_name = aws_autoscaling_group.autoscaling-group.name
# }

# # cloud watch monitoring
# resource "aws_cloudwatch_metric_alarm" "cloudwatch-metric-alarm" {
#   alarm_name                = "cloudwatch-metric-alarm"
#   alarm_description         = "This metric monitors ec2 cpu utilization"
#   comparison_operator       = "GreaterThanOrEqualToThreshold"
#   evaluation_periods        = "2"
#   metric_name               = "CPUUtilization"
#   namespace                 = "AWS/EC2"
#   period                    = "120"
#   statistic                 = "Average"
#   threshold                 = "80"
#   insufficient_data_actions = []
#   dimensions = {
#     AutoScalingGroupName = aws_autoscaling_group.autoscaling-group.name
#   }
#   actions_enabled           = "true"
#   alarm_actions             = [aws_autoscaling_policy.autoscaling-policy.arn]
# }

# # auto descaling policy for
# resource "aws_autoscaling_policy" "autoscaling-policy-scaledown" {
#   name                   = "autoscaling-policy-scaledown"
#   autoscaling_group_name = aws_autoscaling_group.autoscaling-group.name
#   scaling_adjustment     = -1
#   adjustment_type        = "ChangeInCapacity"
#   policy_type            = "SimpleScaling"
#   cooldown               = 60
# }

# # descaling cloud watch

# resource "aws_cloudwatch_metric_alarm" "cloudwatch-metric-alarm-scaledown" {
#   alarm_name                = "cloudwatch-metric-alarm-scaledown"
#   alarm_description         = "This metric monitors ec2 cpu utilization"
#   comparison_operator       = "LessThanOrEqualToThreshold"
#   evaluation_periods        = "2"
#   metric_name               = "CPUUtilization"
#   namespace                 = "AWS/EC2"
#   period                    = "120"
#   statistic                 = "Average"
#   threshold                 = "10"
#   insufficient_data_actions = []
#   dimensions = {
#     AutoScalingGroupName = aws_autoscaling_group.autoscaling-group.name
#   }
#   actions_enabled           = "true"
#   alarm_actions             = [aws_autoscaling_policy.autoscaling-policy-scaledown.arn]
# }